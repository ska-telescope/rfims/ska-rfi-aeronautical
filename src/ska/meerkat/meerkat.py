# -*- coding: utf-8 -*-
"""
    MeerKAT Class and Functions that provide ADC information,
    especially with regard to saturation quantization levels,
    as well as functions to read MeerKAT Histogram Data Files.

    Author: Dr. A. J. Otto
    Organisation: SKA Telescope
    Copyright: 2020 SKA Telescope
    Date: 15 Sept. 2020
"""
from datetime import datetime
import numpy as np
from astropy import units as u


class MeerKAT:
    """
        MeerKAT Class
    """
    # pylint: disable=too-many-instance-attributes 

    def __init__(self):
        print("MeerKAT class __init__():")

        # UNITS
        self.micro_volt = 1E-6 * u.si.volt
        self.milli_volt = 1E-3 * u.si.volt
        self.milli_watt = 1E-3 * u.si.watt
        self.db_m = u.dB(self.milli_watt)
        self.db_w = u.dB(u.si.watt)
        self.db_uv = u.dB(self.micro_volt)

        # CONSTANTS
        self.threshold = 1e-7  # probability threshold
        self.saturation = -70. * self.db_m  # dBm

        # SKA-MID CORE COORDINATES (radians)
        self.ska_lat, self.ska_lon =\
            np.deg2rad(-30.712919), np.deg2rad(21.443800)
        print("...done")

    def adc(self):
        """
            :description
            ____________
            Information on the MeerKAT Analogue-to-Digital Converter (ADC)
            Teledyne e2v AT84AS008; 10-bit 2 GSps
            :returns
            ________
                none
        """
        # ADC CONSTANTS
        self.adc_bits = 10 * u.dimensionless_unscaled
        self.voltage_peak_to_peak = 500. * self.milli_volt
        self.voltage_peak = self.voltage_peak_to_peak / 2

        print("MeerKAT ADC: Teledyne e2v AT84AS008 (10-bit; 2 GSps)\n")
        print("Number of Bits = ",
              self.adc_bits)
        # Determine the number of ADC Quantization Levels:
        self.adc_counts = 2 ** self.adc_bits
        print("ADC Quantization Levels = ",
              self.adc_counts.value)

        print("ADC Voltage = ",
              self.voltage_peak_to_peak, " (peak-to-peak) or ",
              self.voltage_peak, " (peak)")

        # Voltage Scaling Factor is the ratio of the Voltage (peak-to-peak)
        # that can be represented by the ADC Quantization Levels (10-bits)
        self.voltage_scaling_factor =\
            self.voltage_peak / (self.adc_counts / 2)
        print("Voltage Scaling Factor = ",
              self.voltage_scaling_factor, " (= ",
              self.voltage_peak_to_peak / self.adc_counts, ")")

        # Full Scale Voltage is the Maximum Voltage that can be
        # digitized linearly [dBuV]
        self.voltage_full_scale = 20. * np.log10(
            (self.adc_counts.value / 2 * self.voltage_scaling_factor.value)
            / 1e-6) *\
            self.db_uv
        print("Full Scale Voltage = ",
              round(self.voltage_full_scale.value, 2),
              "[", self.voltage_full_scale.unit, "]")
        self.power_full_scale =\
            (self.voltage_full_scale.value - 106.9) * self.db_m

        # Full Scale Power Levels is the Maximum Power Level that
        # can be digitized linearly [dBm]
        print("Full Scale Power (50 ohm) = ",
              round(self.power_full_scale.value, 2),
              " [", self.power_full_scale.unit, "]")

        # Defining start of SATURATION as -3 dBFS:
        self.sat_count_log =\
            20. * np.log10(self.adc_counts) - 3
        self.sat_count_lin =\
            10. ** (self.sat_count_log / 20.)
        print("Possible Saturation Limits = ",
              -self.sat_count_lin / 2,
              self.sat_count_lin / 2)
        print("-3 dBFS Voltage = ",
              self.sat_count_lin * self.voltage_scaling_factor)
        print("\n-3 dBFS = ", -2, " dBm")
        print("\t= ", -2 + 106.9, "dBuV")
        print("\t= ", 10. ** ((-2 + 106.9) / 20.) * 1E-6, "V")
        print("\t= ", (10. ** ((-2 + 106.9) / 20.) * 1E-6) / 500E-3, " ratio")
        print("\t= ", (10. ** ((-2 + 106.9) / 20.) * 1E-6) / 500E-3 * 1024,
              " count")
        return np.floor(self.sat_count_lin)

    def read_histogram(self, data_path, filename, time_data, hist_data):
        """
            :description
            ____________
            Function to Read MeerKAT Histogram Data Files

            :params
            _______
            data_path : str
                location of MeerKAT histogram files
            filename : str
                CSV filename to MeerKAT histogram files to process
            time_data : numpy array
                contains timestamp information for each histogram entry
            hist_data : numpy array
                contains histogram data for a given timestamp

            :returns
            ________
            time_data : numpy array
                contains timestamp information for each histogram entry
            hist_data : numpy array
                contains histogram data for a given timestamp
        """
        self.file_name = data_path + filename
        self.voltage = np.genfromtxt(self.file_name, dtype=np.int32,
                                     comments='#', delimiter=',')
        self.time = self.file_name.split('/')[-1][3:13]
        if np.sum(self.voltage[:, 1]) > 1e7:
            self.time_data = np.append(time_data, int(self.time))
            self.hist_data = np.append(hist_data,
                                       np.reshape(self.voltage[:1024, 1],
                                                  [1024, -1]),
                                       axis=1)
        return self.time_data, self.hist_data

    def time_metadata(self, time_data):
        """
            :description
            ____________
            Acquire Time Metadata from Data Captures

            :params
            _______
                time_data : numpy array
                    contains timestamp information for each histogram entry
            :returns
            ________
                time0 :
                    capture start time
                time1 :
                    capture stop time
                date :
                    capture date
        """
        self.day_start = np.floor(
            np.min(time_data) / (24. * 60. * 60.)) * (24. * 60. * 60.)
        # South Africa Local Time is UTC+2 hours
        self.time_hours = (time_data - self.day_start) / (60. * 60.) + 2.0
        self.capture_time =\
            (self.time_hours[-1] - self.time_hours[0]) * 60. * 60.

        self.date_time = datetime.fromtimestamp(time_data[0])
        self.time0 = self.date_time.strftime("%H:%M:%S")
        self.date_time = datetime.fromtimestamp(time_data[-1])
        self.time1 = self.date_time.strftime("%H:%M:%S")
        self.date = self.date_time.strftime("%d %b, %Y")

        print("\nHistogram Data:")
        print("Capture Date:", self.date)
        print("Start Time: ", self.time0,
              " Stop Time: ", self.time1)
        print("Total Time Capture: ",
              self.capture_time, "seconds\n")
        return self.time0, self.time1, self.date

    def adc_probabilities(self, sat_count_lin, hist_data):
        """
            :description
            ____________
            Determine the probability of ADC quantization levels
            from MeerKAT histogram data.

            :params
            _______
            sat_count_lin:
            hist_data:

            :retruns
            ________
            thresh_prob:
        """
        # ADC Quantization Probabilities > Threshold of 1E-7
        thresh_prob = np.sum(1.0*hist_data/np.sum(hist_data,
                                                  axis=0)
                             > self.threshold,
                             axis=0)

        # Index where Quantization Levels > Sat_Count_Lin (362)
        saturation_index_362 =\
            np.arange(1024)[np.abs(np.arange(-512,
                                             512))
                            > int(sat_count_lin/2)]

        thresh_prob_362 =\
            np.sum(1.0*hist_data[saturation_index_362, :],
                   axis=0)/np.sum(hist_data, axis=0)

        # Index where Quantization Levels > 500 (out of 512)
        saturation_index_500 =\
            np.arange(1024)[np.abs(np.arange(-512,
                                             512))
                            > 500]

        thresh_prob_500 =\
            np.sum(1.0*hist_data[saturation_index_500, :],
                   axis=0)/np.sum(hist_data, axis=0)
        return thresh_prob, thresh_prob_362, thresh_prob_500
