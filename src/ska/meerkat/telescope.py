# -*- coding: utf-8 -*-
"""
    Telescope Class and Functions that provide ADC information,
    especially with regard to saturation quantization levels,
    as well as functions to read MeerKAT Histogram Data Files.

    Author: Dr. A. J. Otto
    Organisation: SKA Telescope
    Copyright: 2020 SKA Telescope
    Date: 15 Sept. 2020
"""
from datetime import datetime
import numpy as np
import scipy.special as sp
import scipy.io
import pymap3d
from astropy import units as u


class Telescope:
    """
        Telescope Class
    """
    # pylint: disable=too-many-instance-attributes

    def __init__(self,
                 verbose=False,
                 band="L-band",
                 freq_centre=None):
        """
            :description
            ____________
            :params
            _______
                freq_centre: Centre Frequency [MHz]
            :returns
            ________
        """
        self.verbose = verbose
        # UNITS
        self.micro_volt = 1E-6 * u.si.volt
        self.milli_volt = 1E-3 * u.si.volt
        self.milli_watt = 1E-3 * u.si.watt
        self.db_m = u.dB(self.milli_watt)
        self.db_w = u.dB(u.si.watt)
        self.db_uv = u.dB(self.micro_volt)

        # CONSTANTS
        self.eps0 = 8.854E-12  # Permittivity
        self.mu0 = 4.*np.pi*1E-7  # Permeability
        # Speed of Light
        self.c0 =\
            1./np.sqrt(self.eps0*self.mu0)  # m/s
        # Wavelength
        self.lambda0 =\
            self.c0/(freq_centre*1E6)  # m
        # Probability Threshold
        self.threshold = 1e-7
        # Telescope Saturation
        self.saturation = -70. * self.db_m  # dBm

        # TELESCOPE COORDINATES (degrees)
        self.telescope_lat, self.telescope_lon =\
            -30.712919, 21.443800
        self.telescope_height = 1052  # meters above sea level

        # TELESCOPE OBSERVER:
        self.ell_wgs84 = pymap3d.Ellipsoid('wgs84')
        lat0, lon0, height0 =\
            self.telescope_lat,\
            self.telescope_lon,\
            self.telescope_height
        # ENU Coordinates (LOCAL)
        self.telescope_e0, self.telescope_n0, self.telescope_u0 =\
            0.0, 0.0, 0.0
        self.telescope_enu_vector =\
            np.array([self.telescope_e0,
                      self.telescope_n0,
                      self.telescope_u0])
        # ECEF Coordinates (GLOBAL)
        self.telescope_x0,\
            self.telescope_y0,\
            self.telescope_z0 =\
            pymap3d.enu2ecef(self.telescope_e0,
                             self.telescope_n0,
                             self.telescope_u0,
                             lat0, lon0, height0,
                             ell=self.ell_wgs84,
                             deg=True)
        # Telescope Observer Vector Array
        self.telescope_vector =\
            np.array([self.telescope_x0,
                      self.telescope_y0,
                      self.telescope_z0])
        self.telescope_unit_vector =\
            self.telescope_vector / np.linalg.norm(self.telescope_vector)
        # Telecsope Gain
        self.rad_theta,\
            self.rad_phi,\
            self.Jones,\
            self.Gmax,\
            self.phi_gain,\
            self.itu_gain_simple,\
            self.phi_gain2,\
            self.itu_gain_bessel =\
            self.telescope_gain_pattern(verbose,
                                        band,
                                        self.lambda0)

    def get_ecef_axis(self, verbose):
        X0, Y0, Z0 =\
            pymap3d.geodetic2ecef(lat=0,
                                  lon=0,
                                  alt=self.telescope_height,
                                  ell=self.ell_wgs84,
                                  deg=True)
        X1, Y1, Z1 =\
            pymap3d.geodetic2ecef(lat=0,
                                  lon=90,
                                  alt=self.telescope_height,
                                  ell=self.ell_wgs84,
                                  deg=True)
        X2, Y2, Z2 =\
            pymap3d.geodetic2ecef(lat=90,
                                  lon=0,
                                  alt=self.telescope_height,
                                  ell=self.ell_wgs84,
                                  deg=True)
        return X0, Y0, Z0,\
            X1, Y1, Z1,\
            X2, Y2, Z2

    def adc(self):
        """
            :description
            ____________
                Information on the MeerKAT Analogue-to-Digital
                Converter (ADC) Teledyne e2v AT84AS008; 10-bit 2 GSps
            :returns
            ________
            none
        """

        # ADC CONSTANTS
        self.adc_bits = 10 * u.dimensionless_unscaled
        self.voltage_peak_to_peak = 500. * self.milli_volt
        self.voltage_peak = self.voltage_peak_to_peak / 2

        # Determine the number of ADC Quantization Levels:
        self.adc_counts = 2 ** self.adc_bits

        # Voltage Scaling Factor is the ratio of the Voltage (peak-to-peak)
        # that can be represented by the ADC Quantization Levels (10-bits)
        self.voltage_scaling_factor =\
            self.voltage_peak / (self.adc_counts / 2)

        # Full Scale Voltage is the Maximum Voltage that can be
        # digitized linearly [dBuV]
        self.voltage_full_scale = 20. * np.log10(
            (self.adc_counts.value / 2 * self.voltage_scaling_factor.value)
            / 1e-6) *\
            self.db_uv

        self.power_full_scale =\
            (self.voltage_full_scale.value - 106.9) * self.db_m

        # Full Scale Power Levels is the Maximum Power Level that
        # can be digitized linearly [dBm]

        # Defining start of SATURATION as -3 dBFS:
        self.sat_count_log =\
            20. * np.log10(self.adc_counts) - 3
        self.sat_count_lin =\
            10. ** (self.sat_count_log / 20.)

        if self.verbose:
            print("\n*****")
            print("Telescope ADC: Teledyne e2v AT84AS008 (10-bit; 2 GSps)")
            print("Number of Bits = ",
                  self.adc_bits)
            print("ADC Quantization Levels = ",
                  self.adc_counts.value)
            print("ADC Voltage = ",
                  self.voltage_peak_to_peak, " (peak-to-peak) or ",
                  self.voltage_peak, " (peak)")
            print("Voltage Scaling Factor = ",
                  self.voltage_scaling_factor, " (= ",
                  self.voltage_peak_to_peak / self.adc_counts, ")")
            print("Full Scale Voltage = ",
                  round(self.voltage_full_scale.value, 2),
                  "[", self.voltage_full_scale.unit, "]")
            print("Full Scale Power (50 ohm) = ",
                  round(self.power_full_scale.value, 2),
                  " [", self.power_full_scale.unit, "]")
            print("Possible Saturation Limits = ",
                  -self.sat_count_lin / 2,
                  self.sat_count_lin / 2)
            print("-3 dBFS Voltage = ",
                  self.sat_count_lin * self.voltage_scaling_factor)
            print("\n-3 dBFS = ", -2, " dBm")
            print("\t= ", -2 + 106.9, "dBuV")
            print("\t= ", 10. ** ((-2 + 106.9) / 20.) * 1E-6, "V")
            print("\t= ", (10. ** ((-2 + 106.9) / 20.) * 1E-6) / 500E-3,
                  " ratio")
            print("\t= ", (10. ** ((-2 + 106.9) / 20.) * 1E-6) / 500E-3 * 1024,
                  " count")
            print("*****\n")
        return np.floor(self.sat_count_lin)

    def read_histogram(self, data_path, filename, time_data, hist_data):
        """
            :description
            ____________
                Function to Read Telescope Histogram Data Files

            :params
            _______
                data_path : str
                    location of Telescope histogram files
                filename : str
                    CSV filename to Telescope histogram files to process
                time_data : numpy array
                    contains timestamp information for each histogram entry
                hist_data : numpy array
                    contains histogram data for a given timestamp

            :returns
            ________
                time_data : numpy array
                    contains timestamp information for each histogram entry
                hist_data : numpy array
                    contains histogram data for a given timestamp
        """
        file_name = data_path + filename
        voltage = np.genfromtxt(file_name, dtype=np.int32,
                                comments='#', delimiter=',')
        time_dat = file_name.split('/')[-1][3:13]
        if np.sum(voltage[:, 1]) > 1e7:
            time_data = np.append(time_data, int(time_dat))
            hist_data = np.append(hist_data,
                                  np.reshape(voltage[:1024, 1],
                                             [1024, -1]),
                                  axis=1)
        return time_data, hist_data

    def time_metadata(self, time_data):
        """
            :description
            ____________
                Acquire Time Metadata from Data Captures

            :params
            _______
                time_data : numpy array
                    contains timestamp information for each histogram entry
            :returns
            ________
                time0 :
                    capture start time
                time1 :
                    capture stop time
                date :
                    capture date
        """
        day_start = np.floor(
            np.min(time_data) / (24. * 60. * 60.)) * (24. * 60. * 60.)
        # South Africa Local Time is UTC+2 hours
        time_hours = (time_data - day_start) / (60. * 60.) + 2.0
        capture_time =\
            (time_hours[-1] - time_hours[0]) * 60. * 60.

        date_time = datetime.fromtimestamp(time_data[0])
        time0 = date_time.strftime("%H:%M:%S")
        date_time = datetime.fromtimestamp(time_data[-1])
        time1 = date_time.strftime("%H:%M:%S")
        date = date_time.strftime("%d %b, %Y")
        print("\n*****")
        print("Histogram Data:")
        print("Capture Date:", date)
        print("Start Time: ", time0,
              " Stop Time: ", time1)
        print("Total Time Capture: ",
              capture_time, "seconds")
        print("*****\n")
        return time0, time1, date

    def adc_probabilities(self, sat_count_lin, hist_data):
        """
            :description
            ____________
                Determine the probability of ADC quantization levels
                from Telescope histogram data.

            :params
            _______
                sat_count_lin:
                hist_data:

            :retruns
            ________
                thresh_prob:
        """
        # ADC Quantization Probabilities > Threshold of 1E-7
        thresh_prob = np.sum(1.0*hist_data/np.sum(hist_data,
                                                  axis=0)
                             > self.threshold,
                             axis=0)

        # Index where Quantization Levels > Sat_Count_Lin (362)
        saturation_index_362 =\
            np.arange(1024)[np.abs(np.arange(-512,
                                             512))
                            > int(sat_count_lin/2)]

        thresh_prob_362 =\
            np.sum(1.0*hist_data[saturation_index_362, :],
                   axis=0)/np.sum(hist_data, axis=0)

        # Index where Quantization Levels > 500 (out of 512)
        saturation_index_500 =\
            np.arange(1024)[np.abs(np.arange(-512,
                                             512))
                            > 500]

        thresh_prob_500 =\
            np.sum(1.0*hist_data[saturation_index_500, :],
                   axis=0)/np.sum(hist_data, axis=0)
        return thresh_prob, thresh_prob_362, thresh_prob_500

    def pointing_vectors(self,
                         M, N,
                         single_pointing,
                         az_pt=None,
                         el_pt=None):
        """
            :description
            ____________
                Obtain the local ENU pointing vectors
            :params
            _______
            :returns
            ________
        """
        # List Azimuth pointing directions
        pointing_az = []
        # List pf Elevation pointing directions
        pointing_el = []

        # Dictionary of ENU pointing vecotrs
        pointing_enu_vector = {}
        # Dictionary of ENU pointing unit vectors
        pointing_enu_unit_vector = {}

        # Iterate over number of pointings
        for num_az in range(M):
            if not single_pointing:
                az_pt = num_az*(360/M)
            pointing_az.append(az_pt)
            for num_el in range(N):
                if not single_pointing:
                    el_pt = num_el*(90/N)
                pointing_el.append(el_pt)

                # Slant Range of Pointing Vector
                srange = 10000
                # Pointing ENU Vector (LOCAL)
                pointing_e, pointing_n, pointing_u =\
                    pymap3d.aer2enu(az_pt,
                                    el_pt,
                                    srange,
                                    deg=True)
                # Pointing Vector (ENU)
                pointing_enu_vector[(az_pt,
                                     el_pt)] =\
                    np.array([pointing_e,
                              pointing_n,
                              pointing_u])
                # Pointing Unit Vector (ENU)
                pointing_enu_unit_vector[(az_pt,
                                          el_pt)] =\
                    pointing_enu_vector[(az_pt,
                                         el_pt)] /\
                    np.linalg.norm(pointing_enu_vector[(az_pt,
                                                        el_pt)])
        return pointing_az, pointing_el,\
            pointing_enu_vector, pointing_enu_unit_vector

    def pathloss(self, filename):
        """
            :description
            ____________
            ITU-R P.528 Pathloss Curve Fit
            :params
            _______
            :retuns
            _______
        """
        pathloss_file = open(filename, "r", encoding="utf-8")
        distance, pathloss = [], []
        for line in pathloss_file:
            try:
                distance.append(float(line.split(',')[0]))
                pathloss.append(float(line.split(',')[1]))
            except  ValueError:
                pass
        return distance, pathloss

    def telescope_gain_pattern(self,
                               verbose,
                               band,
                               lambda0):
        """
            :description
            ____________
                Radiation / Gain Pattern for MeerKAT Telescope
                as computed with CST (<10 GHz) / GRASP (>10 GHz)
            :params
            _______
            :return
            _______
        """
        # Read in CST Computed Gain Data
        # L-Band
        if band == "L-band":
            mat = scipy.io.loadmat(
                "../data/Telescope_Gain_Data/MK_CST_Lband_%d.mat" % 1100)
        # Radiation Pattern Theta (Elevation)
        rad_theta = mat["th"].squeeze()
        # Radiation Pattern Phi (Azimuth)
        rad_phi = mat["ph"].squeeze()

        # Jones Matrix
        # Cross-pol: H-pol feed & V-pol wave
        JHV = mat["Jqv"].squeeze()
        # Co-pol: H-pol feed & H-pol wave
        JHH = mat["Jqh"].squeeze()
        # Co-pol: V-pol feed & V-pol wave
        JVV = mat["Jpv"].squeeze()
        # Cross-pol: V-pol feed & H-pol wave
        JVH = mat["Jph"].squeeze()

        # Normalise patterns to bore-sight
        JHH /= abs(JHH).max()
        JVV /= abs(JVV).max()
        JHV /= abs(JHH).max()
        JVH /= abs(JVV).max()
        Jones = [JHH,
                 JHV,
                 JVH,
                 JVV]
        # Maximum Antenna Gain (Theoretical)
        # Telescope Dish Diameter
        D = 13.5  # m (MeerKAT)
        # Effective Apperture
        Aeff = np.pi*(D/2)**2.
        # Ratio Diameter to Wavelength
        d_wlen = D/lambda0
        # Antenna Efficiency
        eta_a = 1.0
        # Maximum Antenna Gain:
        # Linear
        Gmax_lin =\
            eta_a * (np.pi * D/lambda0)**2.
        # Logarithmic
        Gmax = 10.*np.log10(Gmax_lin)
        Gmax2 = 10.*np.log10((4*np.pi*Aeff)/(lambda0**2.))

        """
        *************
        ITU-R RA.1631
        *************
        Mathematical Model of Avg Radiation Pattern
        non-GSO systems and RAS stations for frequencies
        above 150 MHz
        """
        G1 = -1. + 15.*np.log10(d_wlen)
        # Off-boresight Angle Phi
        phi_m = (20./d_wlen)*np.sqrt(Gmax - G1)
        phi_r = 15.85*((d_wlen)**(-0.6))
        phi_gain = np.linspace(0., 180., 1000)
        # Broadcast Numpy Arrays
        (phi_gain, d_wlen, Gmax, G1, phi_m, phi_r) =\
            np.broadcast_arrays(phi_gain,
                                d_wlen,
                                Gmax,
                                G1,
                                phi_m,
                                phi_r)
        # Simple ITU Gain Model
        itu_gain_simple = np.zeros(phi_gain.shape, np.float64)
        # Case 1:
        mask = (0 <= phi_gain) & (phi_gain < phi_m)
        itu_gain_simple[mask] =\
            Gmax[mask] - 2.5e-3 * (d_wlen[mask] * phi_gain[mask]) ** 2
        # Case 2:
        mask = (phi_m <= phi_gain) & (phi_gain < phi_r)
        itu_gain_simple[mask] = G1[mask]
        # Case 3:
        mask = (phi_r <= phi_gain) & (phi_gain < 10.)
        itu_gain_simple[mask] = 29 - 25. * np.log10(phi_gain[mask])
        # Case 4:
        mask = (10. <= phi_gain) & (phi_gain < 34.1)
        itu_gain_simple[mask] = 34 - 30. * np.log10(phi_gain[mask])
        # Case 5:
        mask = (34.1 <= phi_gain) & (phi_gain < 80.)
        itu_gain_simple[mask] = -12.
        # Case 6:
        mask = (80. <= phi_gain) & (phi_gain < 120.)
        itu_gain_simple[mask] = -7.
        # Case 7:
        mask = (120. <= phi_gain) & (phi_gain <= 180.)
        itu_gain_simple[mask] = -12.
        """
        ****************
        Bessel Functions
        ****************
        More accurate mathematical model of the radiation
        pattern representation of the main-beam for
        frequencies above 150 MHz
        """
        # Off-boresight Angle Phi
        phi_gain2 = np.linspace(0, 20, 1000)
        # Broadcast Numpy Arrays
        (phi_gain2, d_wlen, Gmax, G1, phi_m, phi_r) =\
            np.broadcast_arrays(phi_gain2,
                                d_wlen,
                                Gmax,
                                G1,
                                phi_m,
                                phi_r)
        # Bessel Function ITU Gain Model
        itu_gain_bessel = np.zeros(phi_gain2.shape, np.float64)
        # First Null of Antenna Pattern
        phi_0 = 69.88 / d_wlen
        x = (np.pi*D*phi_gain2)/(360*lambda0)
        # Case 1:
        mask = (0 <= phi_gain2) & (phi_gain2 < phi_0)
        itu_gain_bessel[mask] =\
            Gmax[mask] +\
            20 * np.log10(sp.j1(2 * np.pi * x[mask]) / (np.pi * x[mask]))
        # Case 2:
        mask = (phi_0 <= phi_gain2) & (phi_gain2 < 20.)
        B_sqrt = (10.**(1.6)) * np.pi * d_wlen[mask] / 2.
        itu_gain_bessel[mask] =\
            10.*np.log10(
            B_sqrt * (
                np.cos(2.*np.pi*x[mask] - 3.*np.pi/4. + 0.0953)/(np.pi*x[mask])
                )**2.)
        if verbose:
            print("\n*****")
            print("Telescope Radiation/Gain Pattern:")
            print("Elevation :: Theta = ",
                  np.min(rad_theta),
                  np.max(rad_theta),
                  np.shape(rad_theta),
                  "delta_theta = ",
                  round(np.max(rad_theta)/np.shape(rad_theta)[0], 2),
                  "deg")
            print("Azimuth :: Phi = ",
                  np.min(rad_phi),
                  np.max(rad_phi),
                  np.shape(rad_phi),
                  "delta_phi = ",
                  round(np.max(rad_phi)/np.shape(rad_phi)[0]),
                  "deg\n")
            print("Telescope Dish Diameter = ",
                  D,
                  "m")
            print("Apperture Area = ",
                  round(Aeff, 2),
                  "m**2.")
            print("Maximum Gain = ",
                  round(Gmax2, 2),
                  " dBi\n")
            print("ITU-R RA.1631")
            print("G1 = %.1f dBi" % G1[0])
            print("phi_m = %.1f deg" % phi_m[0])
            print("phi_r = %.1f deg" % phi_r[0])
            print("*****\n")
        return rad_theta, rad_phi, Jones, Gmax2,\
            phi_gain, itu_gain_simple,\
            phi_gain2, itu_gain_bessel

    def telescope_gain(self,
                       alpha,
                       target_az,
                       pointing_az):
        """
            :description
            ____________
                Obtain the telescope gain for a given
                interfrence incident angle Alpha
            :params
            _______
                alpha :
                target_az :
                pointing_az :
            :returns
            ________
                gain
        """
        # Jones Matrix
        # Jones[0] = JHH
        # Jones[1] = JHV
        # Jones[2] = JVH
        # Jones[3] = JVV
        J = self.Jones[3]

        el_theta_index =\
            np.argmin(abs(self.rad_theta - np.rad2deg(alpha)))
        gain = self.Gmax +\
            10.*np.log10(abs(J[el_theta_index, :])) +\
            10.*np.log10(abs(np.conjugate(J[el_theta_index, :])))
        gain_interp =\
            np.interp(abs(target_az - pointing_az),
                      self.rad_phi,
                      gain)
        gain_stats = [np.min(gain),
                      np.percentile(gain, 1),
                      np.percentile(gain, 10),
                      np.percentile(gain, 50),
                      np.percentile(gain, 90),
                      np.percentile(gain, 99),
                      np.max(gain)]
        stats_label = ['Min.', '1%', '10%', '50%', '90%', '99%', 'Max.']
        return gain_stats, gain_interp[0], stats_label
