# -*- coding: utf-8 -*-
"""
    Aircraft Class and Functions that implements aeronautical
    models on known flight paths as RFI sources.

    Author: Dr. A. J. Otto
    Organisation: SKA Telescope
    Copyright: 2020 SKA Telescope
    Date: 24 Sept. 2020
"""
import time
import numpy as np
import pymap3d


class Aircraft:
    """
        Aircraft Class
    """
    def __init__(self,
                 telescope,
                 flightpath_filename,
                 flight_description,
                 power_tx,
                 verbose):
        self.verbose = verbose
        self.flightpath_filename = flightpath_filename
        self.flight_description = flight_description

        # Number of time simulation steps
        self.num_sim = 2000

        # Elipsoid Definition
        self.ell_wgs84 = pymap3d.Ellipsoid('wgs84')

        # Instance of Telescope Class
        self.telescope = telescope

        # Aircraft Characteristics
        self.aircraft_speed = 800.  # km/h or 450 kts
        self.aircraft_speed =\
            (self.aircraft_speed * 1e3) / (60. * 60.)  # m/s

        # Transmit Power of Interference Source
        self.power_tx = power_tx

    def get_lat_lon(self, flightpath_filename, verbose):
        """
            Extract longitude, latitude and altitude from
            FlightRadar24 .csv file of aircraft on specific
            flight path.

            :params
            _______
                flightpath_filename: string
            :returns
            ________
                flight_time: list of floats
                    Timestamps of aircraft on route
                lat: list of floats
                    Latitude of aircraft
                lon: list of floats
                    Longitude of aircraft
                alt: list of floats
                    Altitude of aircraft
        """
        if verbose:
            print("\n*****")
            print("Reading Lat and Lon Flight Path")
        # Latitude and Longitude
        lat, lon = [], []
        # Timestamps of aircraft for flight paths coordinates
        flight_time = []
        # Recorded altitude of aircraft for flight path coordinates
        alt = []
        for line in open(flightpath_filename, 'r'):
            # Read line from .csv file and split at delimeter=','
            tmp = line.split(",")
            try:
                flight_time.append(float(tmp[0]))
                lat.append(float(tmp[3].split("\"")[1]))
                lon.append(float(tmp[4].split("\"")[0]))
                alt.append(float(tmp[5])*0.3048)
            except ValueError:
                if verbose:
                    print("Could not read data and convert to a float...")
                    print("*****\n")
            except FileNotFoundError:
                print("Data file not found...\n")
        return flight_time, lat, lon, alt

    def get_calibrated_distance(self, lat, lon, flight_time):
        """
            Function to calculate distances between GPS
            coordinates (lon, lat)
            :params
            _______
                lat: list of floats
                    degrees
                lon: list of floats
                    degrees
                flight_time: list of timestamps
            :returns
            ________
                flight_mktime: list of readable mktime struct time
                distance:
                delta_t:
                delta_dist:
        """
        # Convert latitiude degrees to radians
        lat = np.deg2rad(lat)
        # Convert longitudinal degrees to radians
        lon = np.deg2rad(lon)

        # Distance list
        distance = []
        # distance2 = []  # second method of distance calculation

        # Time Interval (delta_t) list
        delta_t = []

        # Distance Interval (delta_d) list
        delta_dist = []

        # SKA CORE COORDINATES
        telescope_lat, telescope_lon =\
            self.telescope.telescope_lat, self.telescope.telescope_lon

        # Approximate radius of earth in km
        R_earth = 6371.0

        # Iterate through GPS coordinates of flight path
        for k in range(0, len(lat)):
            # Obtain current GPS coordinates in flight path
            current_lat = lat[k]
            current_lon = lon[k]

            # Difference between core and next GPS coordinates
            delta_lat = current_lat - np.deg2rad(telescope_lat)
            delta_lon = current_lon - np.deg2rad(telescope_lon)

            # Calculate distance between GPS flight path and core coordinate
            a = np.sin(delta_lat / 2)**2 +\
                np.cos(telescope_lat) * np.cos(current_lat) *\
                np.sin(delta_lon / 2)**2
            c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a))

            distance.append(R_earth * c)
            # distance2.append(calc_distance(p1=(lat1, lon1),
            #                                p2=(lat2, lon2)))

            # Calculate delta_d between current and previous GPS coordinate
            if k > 0:
                prev_lon = lon[k-1]
                prev_lat = lat[k-1]
                delta_lat = current_lat - prev_lat
                delta_lon = current_lon - prev_lon

                # Calculate distance between current and previous GPS coordsi
                a = np.sin(delta_lat / 2)**2 +\
                    np.cos(prev_lat) * np.cos(current_lat) *\
                    np.sin(delta_lon / 2)**2
                c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a))
                delta_dist.append(R_earth * c)

        flight_mktime = []
        cnt = 0
        # Iterate through flight timestamps
        for t in flight_time:
            t0 = time.localtime(t)
            # Human readable mktime struct from localtime timestamp
            t1 = time.mktime((t0.tm_year,
                              t0.tm_mon,
                              t0.tm_mday,
                              t0.tm_hour,
                              t0.tm_min,
                              t0.tm_sec,
                              t0.tm_wday,
                              t0.tm_yday,
                              t0.tm_isdst))

            flight_mktime.append(t1)

            # Calculate time interval between current and prev timestamp
            if cnt > 0:
                delta_t.append(flight_mktime[cnt] - flight_mktime[cnt-1])
            cnt += 1
        return flight_mktime, distance, delta_t, delta_dist

    def get_min_distance(self,
                         flight_mktime,
                         distance,
                         flight_description):
        """
            Function to determine the minimum distance
            the aircraft is from the telescope observer
            during its flight path.
            :params
            _______
            :returns
            ________
        """
        # Index where distance between aircraft
        # and telescope observer is at a minimum
        min_dist_index = np.argmin(distance)
        print(flight_description,
              round(distance[min_dist_index], 2),
              "km")
        return flight_mktime[min_dist_index],\
            round(distance[min_dist_index], 2)

    def geographic_distance(self, p1, p2):
        """
            :description
            ____________
            Calculates the Geographic Distance between
            p1=(lat1, lon1) and p2=(lat2, lon2)
            :params
            _______
            :returns
            ________
        """
        # Radius of the Earth
        R = 6371

        lat1, lon1 = p1
        lat2, lon2 = p2

        lat1 = np.deg2rad(lat1)
        lon1 = np.deg2rad(lon1)
        lat2 = np.deg2rad(lat2)
        lon2 = np.deg2rad(lon2)

        phi1 = lat1
        phi2 = lat2

        delta_lat = lat2 - lat1
        delta_lon = lon2 - lon1

        a = self.haversin(delta_lat) +\
            np.cos(phi1) * np.cos(phi2) * self.haversin(delta_lon)
        return 2 * R * np.arctan2(np.sqrt(a), np.sqrt(1 - a))

    def haversin(self, theta):
        return np.sin(0.5 * theta) ** 2

    def get_flightpath(self, verbose):
        """
            Function to extract flight path information and add to
            the aircraft dictionary.

            :params
            _______
                flightpath_filename: string
                flight_description: string
                aircrafts: dictionary
            :returns
            ________
                aircrafts: dictionary
        """
        aircrafts_dict = {}
        # Get flight data from FlightRadar24 .csv
        flight_time0, lat0, lon0, alt0 =\
            self.get_lat_lon(self.flightpath_filename,
                             verbose)

        # Interpolate timestamps to number of simulation steps
        flight_time = np.linspace(flight_time0[0],
                                  flight_time0[-1],
                                  self.num_sim)

        lon_interp = np.interp(flight_time, flight_time0, lon0)
        lat_interp = np.interp(flight_time, flight_time0, lat0)
        alt_interp = np.interp(flight_time, flight_time0, alt0)

        flight_mktime, distance, delta_t, delta_dist =\
            self.get_calibrated_distance(lat_interp,
                                         lon_interp,
                                         flight_time)

        flight_mktime_min, distance_min =\
            self.get_min_distance(flight_mktime,
                                  distance,
                                  self.flight_description)

        aircrafts_dict[self.flight_description] =\
            {'flight_time': flight_time,
             'aircraft_lon': lon_interp,
             'aircraft_lat': lat_interp,
             'aircraft_alt': alt_interp,
             'distance': distance,
             'delta_dist': delta_dist,
             'distance_min': distance_min,
             'flight_mktime': flight_mktime,
             'flight_mktime_min': flight_mktime_min,
             'delta_t': delta_t,
             'flight_description': self.flight_description
             }
        return aircrafts_dict

    def propagate_aircraft(self,
                           data,
                           single_pointing,
                           pointing_az,
                           pointing_el,
                           pointing_enu_vector,
                           pointing_enu_unit_vector):
        """
            :description
            ____________
            Function to propagate the aircraft on
            the defined flight path. Simulation is done
            for each pointing vector.
            :params
            _______
            :returns
            ________
        """
        # Dictionary of received power level for
        # specific Az. and El. angles relative to
        # boresight for each pointing
        power_rx_az_el = {}
        # Dictionary of received power level for
        # El. relative to boresight (any Az.)
        power_rx = {}
        # Dictionary for telescope gain per pointing
        telescope_gain = {}
        telescope_gain_az_el = {}
        # Dictionary for saturation index per pointing
        saturation_index = {}
        for key in pointing_enu_vector:
            # Aircraft (lat, lon)
            aircraft_lon = data['aircraft_lon']
            aircraft_lat = data['aircraft_lat']
            aircraft_alt = data['aircraft_alt']

            aircraft_lon0, aircraft_lat0, aircraft_h0 =\
                aircraft_lon[0],\
                aircraft_lat[0],\
                aircraft_alt[0]
            # Ground Distance between Target and Observer
            dist0 =\
                self.geographic_distance(p1=(aircraft_lat0, aircraft_lon0),
                                         p2=(self.telescope.telescope_lat,
                                         self.telescope.telescope_lon))
            # Flight Path Coordinates
            path_coords = []
            path_coords.append([aircraft_lon0,
                                aircraft_lat0])

            # Aircraft ENU Vector (LOCAL)
            aircraft_e0, aircraft_n0, aircraft_u0 =\
                pymap3d.geodetic2enu(aircraft_lat0,
                                     aircraft_lon0,
                                     aircraft_h0,
                                     self.telescope.telescope_lat,
                                     self.telescope.telescope_lon,
                                     self.telescope.telescope_height,
                                     ell=self.ell_wgs84,
                                     deg=True)

            flight_enu = [[aircraft_e0,
                           aircraft_n0,
                           aircraft_u0]]

            # Aircraft ECEF Vector (GLOBAL)
            aircraft_x0, aircraft_y0, aircraft_z0 =\
                pymap3d.enu2ecef(aircraft_e0,
                                 aircraft_n0,
                                 aircraft_u0,
                                 self.telescope.telescope_lat,
                                 self.telescope.telescope_lon,
                                 self.telescope.telescope_height,
                                 ell=self.ell_wgs84,
                                 deg=True)

            # TARGET VECTOR (GLOBAL)
            target_vector =\
                np.array([aircraft_x0,
                          aircraft_y0,
                          aircraft_z0])

            # TARGET UNIT VECTOR (GLOBAL)
            target_unit_vector =\
                target_vector / np.linalg.norm(target_vector)

            # OBSERVER UNIT VECTOR (GLOBAL)
            observer_unit_vector =\
                self.telescope.telescope_unit_vector

            # THETA (ECEF)
            cos_theta = np.dot(observer_unit_vector,
                               target_unit_vector)
            theta = np.arccos(cos_theta)

            # TARGET ENU VECTOR (LOCAL)
            target_enu_vector =\
                np.array([aircraft_e0,
                          aircraft_n0,
                          aircraft_u0])

            # Aircraft Azimuth, Elevation & Slant Range
            target_az, target_el, target_srange =\
                pymap3d.enu2aer(aircraft_e0,
                                aircraft_n0,
                                aircraft_u0,
                                deg=True)
            flight_az = [target_az]
            flight_el = [target_el]
            flight_srange = [target_srange]

            # TARGET ENU UNIT VECTOR (LOCAL)
            target_enu_unit_vector =\
                target_enu_vector / np.linalg.norm(target_enu_vector)

            # OBSERVER ENU UNIT VECTOR (POINTING)
            observer_enu_unit_vector =\
                pointing_enu_unit_vector[key]

            # ALPHA (ENU)
            cos_alpha = np.dot(observer_enu_unit_vector,
                               target_enu_unit_vector)
            alpha = np.arccos(cos_alpha)

            # Total Flight Distance
            total_distance =\
                np.sum(data['delta_dist']) * 1e3  # m
            # Time Required Complete Flight Path
            time_req =\
                total_distance/self.aircraft_speed

            # Calculate Initial Bearing of Aircraft
            lat1, lon1 = aircraft_lat[0], aircraft_lon[0]
            lat2, lon2 = aircraft_lat[1], aircraft_lon[1]
            delta_lon = lon2 - lon1
            bearing_y = np.sin(delta_lon) * np.cos(lat2)
            bearing_x = np.cos(lat1) * np.sin(lat2) -\
                np.sin(lat1) * np.cos(lat2) * np.cos(delta_lon)
            bearing = np.rad2deg(np.arctan2(bearing_y, bearing_x))

            # ITU-R P.528 Aeronautical Path Loss
            pathloss_dist, pathloss =\
                self.telescope.pathloss(
                    "../data/Pathloss_Data/DME_PathLoss_ITU528_5.csv")
            # Interpolate Curve Fit to Initial Distance
            pathloss_interp =\
                np.interp(dist0,
                          pathloss_dist,
                          pathloss)
            pathloss_atten = [pathloss_interp]
            # Obtain Observer/Telescope Gain for given
            # Alpha angle incident to boresight
            gain_stats, gain_interp, stats_label =\
                self.telescope.telescope_gain(alpha,
                                              target_az,
                                              pointing_az)
            # Received Power at t=0 for interpolated
            # El. and Az. relative to boresight angle
            power_rx_az_el[key] =\
                [self.power_tx - pathloss_interp + gain_interp]
            telescope_gain_az_el[key] = []
            telescope_gain_az_el[key].append(gain_interp)
            # Received Power at t=0 for incident
            # El. to boresight angle
            if single_pointing:
                telescope_gain[key] = []
                telescope_gain[key].append(gain_stats)
                power_rx[key] = []
                power_rx[key].append(
                    self.power_tx - pathloss_interp + telescope_gain[key][0])

            if self.verbose:
                print("\n*****")
                print("Observer Unit Vector =",
                      observer_unit_vector)
                print("Target Unit Vector =",
                      target_unit_vector)
                print("THETA (ECEF) = ",
                      np.rad2deg(theta), " degrees\n")
                print("Observer ENU Unit Vector =",
                      observer_enu_unit_vector)
                print("Target ENU Unit Vector =",
                      target_enu_unit_vector)
                print("ALPHA (ENU) = ",
                      np.rad2deg(alpha), " degrees\n")
                print("Flight Path Total Distance = ",
                      round(total_distance/1e3, 2), "km")
                print("Time Required to Complete Flight Path",
                      round(time_req, 2), "s or ",
                      round(time_req/60, 2), "min")
                print("Ground Distance between Target and Observer = ",
                      round(dist0, 2), " km")
                print("Flight Path Bearing = ",
                      round(bearing, 2), "deg\n")
                print("Telescope Gain = %.2f dB at %.2f deg El.,"
                      "%.2f deg Az. interference signal"
                      % (gain_interp,
                         float(np.rad2deg(alpha)),
                         float(abs(target_az - pointing_az))))
                print("Received Power = %.2f dBm"
                      % (power_rx_az_el[key][0]))
                print("*****\n")

            # Keep track of total simulation distance and time
            sim_dist, sim_time = 0, 0
            flight_dist = [0]
            flight_alpha = [alpha]
            flight_theta = [theta]
            cnt = 0
            for sim_t in data['flight_mktime'][:-1]:
                # Simulation Step Count
                cnt += 1
                # Simulation Time & Distance
                delta_time = data['delta_t'][cnt-1]
                delta_dist = data['delta_dist'][cnt-1]

                sim_dist += delta_dist
                flight_dist.extend([sim_dist])
                sim_time += delta_time
                # Flight Path Coordinates
                aircraft_lon0 = aircraft_lon[cnt]
                aircraft_lat0 = aircraft_lat[cnt]
                aircraft_h0 = aircraft_alt[cnt]
                path_coords.append([aircraft_lon0,
                                    aircraft_lat0])
                # Aircraft ENU Vector (LOCAL)
                aircraft_e0, aircraft_n0, aircraft_u0 =\
                    pymap3d.geodetic2enu(aircraft_lat0,
                                         aircraft_lon0,
                                         aircraft_h0,
                                         self.telescope.telescope_lat,
                                         self.telescope.telescope_lon,
                                         self.telescope.telescope_height,
                                         ell=self.ell_wgs84,
                                         deg=True)

                flight_enu.extend([aircraft_e0,
                                   aircraft_n0,
                                   aircraft_u0])

                # Aircraft ECEF Vector (GLOBAL)
                aircraft_x0, aircraft_y0, aircraft_z0 =\
                    pymap3d.enu2ecef(aircraft_e0,
                                     aircraft_n0,
                                     aircraft_u0,
                                     self.telescope.telescope_lat,
                                     self.telescope.telescope_lon,
                                     self.telescope.telescope_height,
                                     ell=self.ell_wgs84,
                                     deg=True)
                # TARGET VECTOR (GLOBAL)
                target_vector =\
                    np.array([aircraft_x0,
                              aircraft_y0,
                              aircraft_z0])
                # TARGET UNIT VECTOR (GLOBAL)
                target_unit_vector =\
                    target_vector / np.linalg.norm(target_vector)

                # OBSERVER UNIT VECTOR (GLOBAL)
                observer_unit_vector =\
                    self.telescope.telescope_unit_vector

                # THETA (ECEF)
                cos_theta = np.dot(observer_unit_vector,
                                   target_unit_vector)
                theta = np.arccos(cos_theta)
                flight_theta.extend([theta])

                # TARGET ENU VECTOR (LOCAL)
                target_enu_vector =\
                    np.array([aircraft_e0,
                              aircraft_n0,
                              aircraft_u0])

                # Aircraft Azimuth, Elevation & Slant Range
                target_az, target_el, target_srange =\
                    pymap3d.enu2aer(aircraft_e0,
                                    aircraft_n0,
                                    aircraft_u0,
                                    deg=True)
                flight_az.extend([target_az])
                flight_el.extend([target_el])
                flight_srange.extend([target_srange])

                # TARGET ENU UNIT VECTOR (LOCAL)
                target_enu_unit_vector =\
                    target_enu_vector / np.linalg.norm(target_enu_vector)

                # OBSERVER ENU UNIT VECTOR (POINTING)
                observer_enu_unit_vector =\
                    pointing_enu_unit_vector[key]

                # ALPHA (ENU)
                cos_alpha = np.dot(observer_enu_unit_vector,
                                   target_enu_unit_vector)
                alpha = np.arccos(cos_alpha)
                flight_alpha.extend([alpha])
                # Obtain Observer/Telescope Gain for given
                # Alpha angle incident to bore-sight
                gain_stats, gain_interp, stats_label =\
                    self.telescope.telescope_gain(alpha,
                                                  target_az,
                                                  pointing_az)
                # Interpolate Curve Fit to Current Distance
                pathloss_interp =\
                    np.interp(abs(dist0-sim_dist),
                              pathloss_dist,
                              pathloss)
                pathloss_atten.append(pathloss_interp)
                # Received Power at t = sim_t
                power_rx_az_el[key].append(
                    self.power_tx - pathloss_interp + gain_interp)
                telescope_gain_az_el[key].append(gain_interp)
                if single_pointing:
                    telescope_gain[key].append(gain_stats)
                    power_rx[key].append(
                        self.power_tx -
                        pathloss_interp +
                        telescope_gain[key][cnt])

                # Debugging Outputs
                if not self.verbose:
                    print("\n*****")
                    print("Timestamp = ", time.ctime(sim_t))
                    print("Simulation Distance = %.2f [km]" % (sim_dist))
                    print("Simulation Time = %.2f [s]" % (sim_time))
                    print("Pathloss = %.2f [dB]" % (pathloss_interp))
                    print("Telescope Gain = %.2f dB at %.2f deg El.,"
                          "%.2f deg Az. interference signal"
                          % (gain_interp,
                             float(np.rad2deg(alpha)),
                             float(abs(target_az - pointing_az))))
                    print("Received Power = %.2f dBm"
                          % (power_rx_az_el[key][cnt]))
                    print("*****\n")
                    input("<next>")

            saturation_index[key] =\
                np.where(
                    power_rx_az_el[key] >= self.telescope.saturation.value)

        flight_info = {}
        flight_info['flight_dist'] = flight_dist
        flight_info['flight_az'] = flight_az
        flight_info['flight_el'] = flight_el
        flight_info['flight_srange'] = flight_srange
        flight_info['dist0'] = dist0
        flight_info['flight_alpha'] = np.rad2deg(flight_alpha)
        flight_info['flight_theta'] = np.rad2deg(flight_theta)
        flight_info['pathloss_atten'] = pathloss_atten

        telescope_info = {}
        telescope_info['pointing_az'] = pointing_az
        telescope_info['pointing_el'] = pointing_el
        telescope_info['telescope_gain'] =\
            telescope_gain[key]
        telescope_info['telescope_gain_az_el'] =\
            telescope_gain_az_el[key]

        return power_rx, power_rx_az_el, stats_label,\
            saturation_index, flight_info, telescope_info
