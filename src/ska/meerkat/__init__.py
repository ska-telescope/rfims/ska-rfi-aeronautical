# -*- coding: utf-8 -*-

"""Module init code."""


__version__ = '0.0.0'

__author__ = 'Dr. A. J. Otto'
__email__ = 'B.Otto@skatelescope.org'

from .telescope import Telescope
from .aircraft import Aircraft
