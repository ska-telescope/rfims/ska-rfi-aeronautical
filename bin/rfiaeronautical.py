# -*- coding: utf-8 -*-
"""
    :description
    ____________
    'rfiaeronautical' is an executable script that imports MeerKAT
    and Aircraft classes and functions to determine the impact of
    aeronautical RFI on the telescope receiver. Measurement data
    from the MeerKAT telescope is compared to simulated data using
    FlightRadar24 flight paths of actual aircraft during the various
    measurement campaigns.

    Author: Dr. A. J. Otto
    Organisation: SKA Telescope
    Copyright: 2020 SKA Telescope
    Date: 15 Sept. 2020
"""
import os
import time
import itertools

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import gridspec
import astropy.units as u
import pymap3d

import ska.meerkat.telescope as mk
import ska.meerkat.aircraft as ac


def get_histograms(telescope, data_path):
    """
        :description
        ____________
            READ MEERKAT HISTOGRAM DATA
        :params
        _______
            telescope : class instance
                instance of Telescope class
            data_path : string
                folder where MeerKAT histograms are located
        :returns
        ________
            time_data : numpy array
                contains timestamp information for each histogram entry
            hist_data : numpy array
                contains histogram data for a given timestamp
            time0 : strftime("%H:%M:%S")
                capture start time
            time1 : strftime("%H:%M:%S")
                capture stop time
            date : strftime("%d %b, %Y")
                capture date
    """
    file_list = os.listdir(data_path)
    file_list.sort()

    # READ DATA FROM ALL CSV FILES IN FOLDER
    hist_data = np.empty([1024, 0])
    time_data = np.empty(0)
    for file_name in file_list:
        if "csv" in file_name:
            time_data, hist_data = telescope.read_histogram(data_path,
                                                            file_name,
                                                            time_data,
                                                            hist_data)

    # ACQUIRE TIME METADATA
    time0, time1, date = telescope.time_metadata(time_data)
    return time_data, hist_data, time0, time1, date


def get_figure(nrows=1, ncols=[1],
               projection=None):
    """
    :description
    ____________
        Function to produce a matplotlib pyplot
        figure with gridspec layout of ncols and nrows
    :params
    _______
        nrows: number of rows in plot
            int >= 1
        ncols: number of columns per row
            list of integers
    :returns
    ________
        fig_ax : list of figure axes
    """
    fig = plt.figure(figsize=(12, 7),
                     tight_layout=True)
    gspec = gridspec.GridSpec(ncols=np.max(ncols),
                              nrows=np.max(nrows),
                              figure=fig)
    fig_ax = []
    cnt = 0
    for row in range(0, nrows):
        for col in range(0, ncols[row]):
            # Create figure axis
            if ncols[row] == 1:
                fig_ax.append(fig.add_subplot(gspec[row, :],
                                              projection=projection[cnt]))
            else:
                fig_ax.append(fig.add_subplot(gspec[row, col],
                                              projection=projection[cnt]))

            if projection[cnt] is None:
                fig_ax[cnt].grid(True, 'both')
            cnt += 1
    return fig_ax


def plot_rfi_flight(flight_info,
                    telescope_info,
                    stats_label,
                    data):
    """
        :description
        :params
        :returns
    """
    fig_ax = get_figure(nrows=2,
                        ncols=[2, 2],
                        projection=[None, None, None, None])
    # Azimuth, Elevation and Slant Range of Aircraft
    p_az, =\
        fig_ax[0].plot(flight_info['flight_dist'],
                       flight_info['flight_az'])
    p_el, =\
        fig_ax[0].plot(flight_info['flight_dist'],
                       flight_info['flight_el'])

    # Index where telescope pointing (Az.) is
    # equal to the flight path Az. angle relative
    # to telescope obersver
    az_index = np.argmin(abs(
        np.array(flight_info['flight_az']) -
        telescope_info['pointing_az']))
    dist1 = flight_info['flight_dist'][az_index]
    time1 = data['flight_mktime'][az_index]
    print('Aircraft in Az. boresight at distance = %.2f'
          ' (Telescope at D = %.2f) on %s\n'
          % (dist1,
             flight_info['dist0'],
             time.ctime(time1)))

    fig_ax[0].axvline(dist1,
                      color='steelblue',
                      linewidth=1,
                      alpha=0.5,
                      linestyle="solid")
    fig_ax[0].axvline(flight_info['dist0'],
                      color='red',
                      linewidth=1,
                      alpha=0.5,
                      linestyle="dashed")
    fig_ax[0].plot(flight_info['dist0'],
                   telescope_info['pointing_az'],
                   c='steelblue', marker='o')
    fig_ax[0].plot(flight_info['dist0'],
                   telescope_info['pointing_el'],
                   c='#ff7f0e', marker='o')
    range_ax = fig_ax[0].twinx()
    p_srange, =\
        range_ax.plot(flight_info['flight_dist'],
                      np.array(flight_info['flight_srange'])/1e3,
                      '--m', linewidth=1, alpha=0.7)
    fig_ax[0].set_title('Flight Path Relative to Telescope Observer')
    fig_ax[0].set_xlabel('Distance [km]')
    fig_ax[0].set_ylabel('Angle [deg]')
    range_ax.set_ylabel('Slant Range [km]')
    fig_ax[0].legend([p_az, p_el, p_srange],
                     ["Az. [deg]",
                      "El. [deg]",
                      "Slant Range [km]"],
                     fontsize=7,
                     loc="center left")
    # Theta and Alpha Angles
    # (Flight Path relative to Telescope)
    p_theta, =\
        fig_ax[1].plot(flight_info['flight_dist'],
                       flight_info['flight_alpha'],
                       '--g', linewidth=1)
    p_alpha, =\
        fig_ax[1].plot(flight_info['flight_dist'],
                       flight_info['flight_theta'],
                       '--r', linewidth=1)
    fig_ax[1].set_xlabel('Distance [km]')
    fig_ax[1].set_ylabel('Angle [deg]')
    fig_ax[1].set_title(
        'Flight Path Relative to Telescope Pointing (Az.=%.1f,El.=%.1f)'
        % (telescope_info['pointing_az'][0],
           telescope_info['pointing_el'][0]))
    fig_ax[1].legend([p_theta, p_alpha],
                     ["Theta [deg]",
                      "Alpha [deg]"],
                     loc=2,
                     fontsize=7)

    # ITU-R P.528 Aeronautical Pathloss
    # Receiver / Telescope Gain
    p_atten, =\
        fig_ax[2].plot(flight_info['flight_dist'],
                       flight_info['pathloss_atten'],
                       '--k', linewidth=1,
                       alpha=0.5)
    fig_ax[2].set_xlabel('Distance [km]')
    fig_ax[2].set_ylabel('Attenuation [dB]')
    fig_ax[2].set_title('ITU-R P.528 Aeronautical Pathloss')
    # gain_ax = fig_ax[2].twinx()
    fig_ax[2].legend([p_atten],
                     ['ITU-R P.528'],
                     fontsize=7,
                     loc='best')

    # Telescope Radiation Gain
    gains = np.array(telescope_info['telescope_gain'])
    num_stats = np.shape(gains)[1]
    cmap = plt.get_cmap('jet')
    legend_handles = []
    legend_labels = []
    # Plot Gain Statistics
    for stats in range(0, num_stats):
        color = cmap(float(stats)/num_stats)
        p_gain, = fig_ax[3].plot(flight_info['flight_dist'],
                                 gains[:, stats],
                                 '-', color=color, alpha=0.4, linewidth=1)
        legend_handles.append(p_gain)
        legend_labels.append(stats_label[stats] + ' Gain')
    comp, =\
        fig_ax[3].plot(flight_info['flight_dist'],
                       telescope_info['telescope_gain_az_el'],
                       '-r')
    legend_handles.append(comp)
    legend_labels.append('Computed (Az., El.)')
    fig_ax[3].legend(legend_handles,
                     legend_labels,
                     fontsize=7,
                     loc='best')
    fig_ax[3].set_xlabel('Distance [km]')
    fig_ax[3].set_ylabel('Gain [dB]')
    fig_ax[3].set_title("Telescope Gain Pointing (Az.=%.1f,El.=%.1f)"
                        % (telescope_info['pointing_az'][0],
                           telescope_info['pointing_el'][0]))
    plt.show()
    return az_index


def plot_power_rx(power_rx,
                  power_rx_az_el,
                  flight_dist,
                  stats_label,
                  dist0,
                  saturation,
                  saturation_index):
    """
        :description
        ____________
        :params
        _______
        :returns
        ________
    """
    # Obtain new figure axes
    fig_ax = get_figure(nrows=1, ncols=[1],
                        projection=[None])
    for key in power_rx:
        sat_ind = saturation_index[key]
        azimuth_pt, elevation_pt =\
            key[0], key[1]
        pwr_rx = np.array(power_rx[key])
        num_stats = np.shape(pwr_rx)[1]
        cmap = plt.get_cmap('jet')
        legend_handles = []
        legend_labels = []
        # Plot Gain Statistics
        for stats in range(0, num_stats):
            color = cmap(float(stats)/num_stats)
            p_pwr, = fig_ax[0].plot(flight_dist,
                                    pwr_rx[:, stats],
                                    '-', color=color, alpha=0.4, linewidth=1)
            legend_handles.append(p_pwr)
            legend_labels.append(stats_label[stats] + ' Gain')
        # Plot Computed Az. El. Gain
        comp, = fig_ax[0].plot(flight_dist,
                               power_rx_az_el[key],
                               '-r', linewidth=2)
        legend_handles.append(comp)
        legend_labels.append("Computed (Az., El.)")
        # Lower Saturation Limit
        fig_ax[0].axvline(np.array(flight_dist)[sat_ind[0][0]],
                          color='magenta',
                          linewidth=1.25,
                          alpha=0.75,
                          linestyle="dashed")
        # Upper Saturation Limit
        fig_ax[0].axvline(np.array(flight_dist)[sat_ind[0][-1]],
                          color='magenta',
                          linewidth=1.25,
                          alpha=0.75,
                          linestyle="dashed")
        print("Computed Saturation Radiuses %.1f km to %.1f km"
              % (abs(dist0-np.array(flight_dist)[sat_ind[0][0]]),
                 abs(dist0-np.array(flight_dist)[sat_ind[0][-1]])))
        # fig_ax[0].plot(np.array(flight_dist)[sat_ind],
        #                np.array(power_rx_az_el[key])[sat_ind],
        #                'k.', markersize=2)
    fig_ax[0].legend(legend_handles,
                     legend_labels,
                     fontsize=7,
                     loc='best')
    fig_ax[0].axvline(dist0, color='black',
                      linewidth=1.25, alpha=0.75, linestyle='dashed')
    fig_ax[0].axhline(saturation, color='magenta',
                      linewidth=1.25, alpha=0.75, linestyle='dashed')
    fig_ax[0].set_xlabel('Distance [km]')
    fig_ax[0].set_ylabel('Power [dBm]')
    fig_ax[0].set_title("Received Power at Input to LNA: "
                        "Pointing (Az.=%.1f$^o$,El.=%.1f$^o$)"
                        % (azimuth_pt, elevation_pt))
    plt.show()


def plot_flightpaths(aircrafts_dict,
                     telescope_lon, telescope_lat,
                     flight_title,
                     time_data,
                     thresh_prob,
                     thresh_title,
                     time_axis,
                     time_label):
    """
        :description
        ____________
            PLOT
        :params
        _______
        :returns
        _______
    """
    number_aircraft =\
        len(aircrafts_dict.keys())
    cmap = plt.get_cmap('viridis')
    color = iter(
        cmap(np.linspace(0,
                         1,
                         number_aircraft)))
    marker = itertools.cycle(('p', 's', 'o',
                              'v', '^', '<',
                              '>', '1', '2'))
    # Obtain new figure axes
    fig_ax = get_figure(nrows=2, ncols=[1, 1],
                        projection=[None, None])

    # SKA Virtual Core Coordinates
    p1, = fig_ax[0].plot(telescope_lon,
                         telescope_lat,
                         color='g', marker='s',
                         alpha=0.5)
    dist_ax = fig_ax[1].twinx()
    legend_labels = [[p1, 'Telescope Virtual Core']]
    # Aircraft Paths
    for key in aircrafts_dict:
        data = aircrafts_dict[key]
        color = next(color)
        marker = next(marker)
        p0, = fig_ax[0].plot(data['aircraft_lon'],
                             data['aircraft_lat'],
                             color=color,
                             marker=marker,
                             markersize=4,
                             alpha=0.5)
        dist_ax.plot(data['flight_time'],
                     data['distance'],
                     '--', color=color,
                     alpha=0.5)
        dist_ax.text(data['flight_time'][-1],
                     data['distance'][-1] + 30,
                     data['flight_description'],
                     color=color)
        dist_ax.text(data['flight_mktime_min'],
                     data['distance_min'] + 100,
                     str(data['distance_min'])+"km",
                     color=color, rotation=90)
        legend_labels.append([p0,
                              data['flight_description']])

    fig_ax[0].set_xlabel('Longitude [$^o$]')
    fig_ax[0].set_ylabel('Latitude [$^o$]')
    fig_ax[0].set_title(flight_title)

    legend_labels = np.array(legend_labels)
    fig_ax[0].legend(legend_labels[:, 0],
                     legend_labels[:, 1],
                     fontsize=8, loc=2)

    # Threshold Probability
    fig_ax[1].plot(time_data,
                   thresh_prob,
                   color='steelblue',
                   alpha=0.7)
    fig_ax[1].set_xlabel('SAST Time')
    fig_ax[1].set_ylabel('ADC Voltage Count')
    fig_ax[1].set_title(thresh_title)

    dist_ax.set_ylabel('Distance from Telescope Virtual Core [km]')
    fig_ax[1].set_xticks(time_axis)
    fig_ax[1].set_xticklabels(time_label,
                              rotation=90,
                              fontsize=8)
    plt.show()


def plot_telescope_gain(phi_gain,
                        itu_gain_simple,
                        phi_gain2,
                        itu_gain_bessel,
                        rad_theta,
                        Jones,
                        Gmax):
    """
        :description
        ____________
        :params
        _______
        :returns
        ________
    """
    # Obtain new figure axes
    fig_ax = get_figure(nrows=1, ncols=[1],
                        projection=[None])
    # Simplified ITU-R RA.1631
    plot_simple, = fig_ax[0].semilogx(phi_gain,
                                      itu_gain_simple,
                                      '-k', linewidth=1.5,
                                      alpha=0.7)
    # Bessel Function ITU-R RA.1631
    plot_bessel, = fig_ax[0].semilogx(phi_gain2,
                                      itu_gain_bessel,
                                      '--r',
                                      alpha=0.7)
    jones = Jones[3]
    num_colours = int(np.shape(jones)[1])
    cmap = plt.get_cmap('jet_r')
    for num_elem in range(0, np.shape(jones)[1]):
        if num_elem % 10 == 0:
            color = cmap(float(num_elem)/num_colours)
            gain = Gmax +\
                10.*np.log10(abs(jones[:, num_elem])) +\
                10.*np.log10(abs(np.conjugate(jones[:, num_elem])))
            fig_ax[0].semilogx(rad_theta,
                               gain,
                               color=color,
                               alpha=0.5,
                               linewidth=1)
    fig_ax[0].legend([plot_simple,
                      plot_bessel],
                     ["ITU-R RA.1632",
                      "Bessel Function"])
    fig_ax[0].set_title('Telescope Antenna Gain')
    fig_ax[0].set_xlabel('Phi: Degrees off-boresight [$^o$]')
    fig_ax[0].set_ylabel('Gain [dBi]')
    plt.show()


def plot_saturation(flight_description,
                    time_data,
                    thresh_prob_362,
                    thresh_prob_500,
                    time_axis,
                    time_label,
                    aircrafts_dict,
                    verbose,
                    az_index):
    """
        :description
        ____________
            PLOT
        :params
        _______
        :returns
        ________
    """
    # Data for 'flight_description' from
    # aircrafts dictionary
    data = aircrafts_dict[flight_description]

    # Obtain new figure axes
    fig_ax = get_figure(nrows=1, ncols=[1],
                        projection=[None])
    fig_ax[0].set_xticks(time_axis)
    fig_ax[0].set_xticklabels(time_label,
                              rotation=90,
                              fontsize=8)
    # Plot probability threshold for
    # ADC counts > 500 (of 512)
    fig_ax[0].semilogy(time_data,
                       thresh_prob_500,
                       '--r', alpha=0.5)
    # Plot probability threshold for
    # ADC counts > 362 (saturation count)
    fig_ax[0].semilogy(time_data,
                       thresh_prob_362,
                       '--g', alpha=0.5)
    # Human readable time where distance
    # of aircraft to site is at minimum
    # time_min =\
    #     data['flight_mktime_min']
    time_min =\
        data['flight_mktime'][az_index]
    # Index where distance of aircraft
    # to the site is at a minimum
    min_ind = np.argmin(abs(time_data - time_min))
    min_ind2 = np.argmin(abs(data['flight_time'] - time_min))
    sat_ind_ll =\
        min_ind -\
        np.where(thresh_prob_500[min_ind::-1] <= 1e-6)[0][0]
    sat_ind_ul =\
        min_ind +\
        np.where(thresh_prob_500[min_ind:] <= 1e-6)[0][0]
    min_ind2_ll =\
        np.argmin(abs(data['flight_time']-time_data[sat_ind_ll+1]))
    min_ind2_ul =\
        np.argmin(abs(data['flight_time']-time_data[sat_ind_ul-1]))

    # Plot threshold at min_ind
    fig_ax[0].plot(time_data[min_ind],
                   thresh_prob_500[min_ind],
                   'ro')
    fig_ax[0].plot(time_data[sat_ind_ll+1],
                   thresh_prob_500[sat_ind_ll+1],
                   'ro')
    fig_ax[0].plot(time_data[sat_ind_ul-1],
                   thresh_prob_500[sat_ind_ul-1],
                   'ro')
    fig_ax[0].set_xlim(time_min - 2000.,
                       time_min + 2000.)
    fig_ax[0].set_xlabel('SAST Time')
    dist_ax = fig_ax[0].twinx()
    # Flight Distance as function of Flight Time
    dist_ax.plot(data['flight_time'],
                 data['distance'],
                 '--', color='k',
                 alpha=0.5)
    dist_ax.plot(data['flight_time'][min_ind2],
                 data['distance'][min_ind2],
                 'rx')
    dist_ax.plot(data['flight_time'][min_ind2_ll],
                 data['distance'][min_ind2_ll],
                 'rx')
    dist_ax.plot(data['flight_time'][min_ind2_ul],
                 data['distance'][min_ind2_ul],
                 'rx')
    dist_ax.plot(data['flight_time'][az_index],
                 data['distance'][az_index],
                 'gs')
    dist_ax.axvline(data['flight_time'][az_index],
                    color='steelblue',
                    linewidth=1,
                    alpha=0.75,
                    linestyle="dashed")
    if verbose:
        print("\n*****")
        print("Measured Saturation Radiuses %.1f km to %.1f km"
              % (data['distance'][min_ind2_ll],
                 data['distance'][min_ind2_ul]))
        print("*****\n")
    time_str =\
        str(data['distance_min'])+" km (" +\
        str(time.ctime(data['flight_mktime_min'])) + ")"
    dist_ax.text(data['flight_mktime_min'],
                 data['distance_min'] + 100,
                 time_str,
                 color='k', rotation=90)
    dist_ax.set_ylabel('Distance from SKA Virtual Core [km]')
    plt.show()


def plot_reference(X0, Y0, Z0,
                   X1, Y1, Z1,
                   X2, Y2, Z2,
                   telescope):
    """
    """
    # Elevation and Azimuth for Earth Reference
    theta = np.linspace(0*u.deg, 180*u.deg, 50)
    phi = np.linspace(0*u.deg, 360*u.deg, 50)
    THETA, PHI = np.meshgrid(theta, phi)

    # Earth Radius
    # SPHERICAL TO CARTESIAN
    Re = 6371E3
    Xe = Re * np.sin(THETA) * np.cos(PHI)
    Ye = Re * np.sin(THETA) * np.sin(PHI)
    Ze = Re * np.cos(THETA)

    # Obtain new figure axes
    fig_ax = get_figure(nrows=1,
                        ncols=[2],
                        projection=['3d', '3d'])
    # Plot Earth Sphere
    fig_ax[0].plot_wireframe(Xe, Ye, Ze,
                             linewidth=0.5,
                             alpha=0.5)
    # ECEF Reference Frame
    # X-Axis
    fig_ax[0].plot([0, X0],
                   [0, Y0],
                   [0, Z0],
                   '-r')
    fig_ax[0].text(X0, Y0, Z0, "X", color='r')
    # Y-Axis
    fig_ax[0].plot([0, X1],
                   [0, Y1],
                   [0, Z1],
                   '-g')
    fig_ax[0].text(X1, Y1, Z1, "Y", color='g')
    # Z-Axis
    fig_ax[0].plot([0, X2],
                   [0, Y2],
                   [0, Z2],
                   '-b')
    fig_ax[0].text(X2, Y2, Z2, "Z", color='b')

    # Telescope Position (ECEF)
    fig_ax[0].plot([0, telescope.telescope_x0],
                   [0, telescope.telescope_y0],
                   [0, telescope.telescope_z0],
                   '-k')
    fig_ax[0].text(telescope.telescope_x0,
                   telescope.telescope_y0,
                   telescope.telescope_z0,
                   "Telescope", color='k',
                   fontsize=6)
    # Axis Properties
    fig_ax[0].axis('off')
    fig_ax[0].set_xticks([])
    fig_ax[0].set_yticks([])
    fig_ax[0].set_zticks([])
    fig_ax[0].view_init(azim=45, elev=0)
    fig_ax[0].set_title("Aeronautical RFI (ECEF Reference Frame)")

    # ENU Reference Frame
    e0 = telescope.telescope_e0
    n0 = telescope.telescope_n0
    u0 = telescope.telescope_u0
    # North (N-Axis)
    e_point, n_point, u_point =\
        pymap3d.aer2enu(az=0,
                        el=0,
                        srange=50000,
                        deg=True)
    fig_ax[1].plot([e0, e0+e_point],
                   [n0, n0+n_point],
                   [u0, u0+u_point],
                   '-r')
    fig_ax[1].text(e0+e_point,
                   n0+n_point,
                   u0+u_point,
                   "n", color='r')
    # Up (U-Axis)
    e_point, n_point, u_point =\
        pymap3d.aer2enu(az=0,
                        el=90,
                        srange=50000,
                        deg=True)
    fig_ax[1].plot([e0, e0+e_point],
                   [n0, n0+n_point],
                   [u0, u0+u_point],
                   '-g')
    fig_ax[1].text(e0+e_point,
                   n0+n_point,
                   u0+u_point,
                   "u", color='g')
    # East (E-Axis)
    e_point, n_point, u_point =\
        pymap3d.aer2enu(az=90,
                        el=0,
                        srange=50000,
                        deg=True)
    fig_ax[1].plot([e0, e0+e_point],
                   [n0, n0+n_point],
                   [u0, u0+u_point],
                   '-b')
    fig_ax[1].text(e0+e_point,
                   n0+n_point,
                   u0+u_point,
                   "e", color='b')
    fig_ax[1].view_init(azim=52, elev=26)
    fig_ax[1].set_title("Aeronautical RFI (ENU Reference Frame)")
    plt.show()



def main():
    """
        main function for rfiaeronautical.py
    """
    os.system("cls||clear")
    verbose = True

    print("ska-rfi-aeronautical/rfiaeronautical:main\n")
    # Define SINGLE POINTING simulation
    single_pointing = True

    if single_pointing:
        # Pointing #1: Az=93.8, El=14,8
        # Azimuth
        az_pointing = 93.8  # degrees
        # Elevation
        el_pointing = 14.8  # degrees

        num_az_pointings = 1
        num_el_pointings = 1
    else:
        num_az_pointings = 50
        num_el_pointings = 50

    # Interference Source
    # ADS-B / SSR
    # Centre Frequency
    freq_centre = 1090.  # MHz
    # Linear Transmit Power
    power_tx_lin = 300.  # Watt
    power_tx =\
        10.*np.log10(power_tx_lin/1e-3)

    # Instance of Telescope Class
    telescope = mk.Telescope(verbose,
                             "L-band",
                             freq_centre)

    # MeerKAT Pointing Vectors
    pointing_az, pointing_el,\
        pointing_enu_vector,\
        pointing_enu_unit_vector =\
        telescope.pointing_vectors(num_az_pointings,
                                   num_el_pointings,
                                   single_pointing,
                                   az_pointing,
                                   el_pointing)
    # ECEF Axis
    X0, Y0, Z0,\
        X1, Y1, Z1,\
        X2, Y2, Z2 = telescope.get_ecef_axis(verbose)
    # Plot Reference Frames and Axis
    plot_reference(X0, Y0, Z0,
                   X1, Y1, Z1,
                   X2, Y2, Z2,
                   telescope)

    # Information on MeerKAT e2v ADC
    sat_count_lin = telescope.adc()

    # Folder containing MeerKAT Histogram Data for Flight Campaigns:
    # 4 February 2019: ./histograms4
    #    8 April 2019: ./histograms5
    #   30 April 2019: ./histograms6
    #      2 May 2019: ./histograms7
    data_path = '../data/MeerKAT_Data/histograms5/'
    time_data, hist_data, time0, time1, date =\
        get_histograms(telescope,
                       data_path)

    # Human Readable Time Axis for Plot
    time_axis = np.linspace(time_data[0],
                            time_data[-1],
                            10)
    time_label = [time.ctime(t) for t in time_axis]

    # String Title for Probability Theshold Plot
    thresh_title =\
        'Peak-to-Peak ADC Voltage: ' + str(date) +\
        ' (' + str(time0) + ' to ' + str(time1) + ')'

    # From MeerKAT histogram data determine
    # probability of quantization levels
    thresh_prob, thresh_prob_362, thresh_prob_500 =\
        telescope.adc_probabilities(sat_count_lin,
                                    hist_data)

    # Telescope Gain Pattern
    plot_telescope_gain(telescope.phi_gain,
                        telescope.itu_gain_simple,
                        telescope.phi_gain2,
                        telescope.itu_gain_bessel,
                        telescope.rad_theta,
                        telescope.Jones,
                        telescope.Gmax)

    # Dictionary of Aircrafts to consider
    aircrafts_dict = {}
    flight_data_path = '../data/Flight_Data/'

    # FlightPath 1 (8 Apr 2019)
    flight_description = "Kulula (MN493)"
    flight_title =\
        flight_description + " Flight Path"
    flightpath_filename =\
        flight_data_path + 'MN493_20154c16.csv'

    # Instance of Aircraft Class
    aircraft = ac.Aircraft(telescope,
                           flightpath_filename,
                           flight_description,
                           power_tx,
                           verbose)

    # Aircraft Flight Path
    aircrafts_dict = aircraft.get_flightpath(verbose)
    # Plot Flight Path Data
    plot_flightpaths(aircrafts_dict,
                     telescope.telescope_lon,
                     telescope.telescope_lat,
                     flight_title,
                     time_data,
                     thresh_prob,
                     thresh_title,
                     time_axis,
                     time_label)

    # Propagate Aircraft on Flight Path:
    # Data for 'flight_description'
    # from aircrafts dictionary
    data = aircrafts_dict[flight_description]
    power_rx,\
        power_rx_az_el,\
        stats_label,\
        saturation_index,\
        flight_info,\
        telescope_info =\
        aircraft.propagate_aircraft(data,
                                    single_pointing,
                                    pointing_az,
                                    pointing_el,
                                    pointing_enu_vector,
                                    pointing_enu_unit_vector)
    # Plot RFI Flight Information
    az_index = plot_rfi_flight(flight_info,
                               telescope_info,
                               stats_label,
                               data)

    # Plot Received Power at Input to LNA
    plot_power_rx(power_rx,
                  power_rx_az_el,
                  flight_info['flight_dist'],
                  stats_label,
                  flight_info['dist0'],
                  telescope.saturation.value,
                  saturation_index)

    # Obtain saturation threshold radiuses from plots
    plot_saturation(flight_description,
                    time_data,
                    thresh_prob_362,
                    thresh_prob_500,
                    time_axis,
                    time_label,
                    aircrafts_dict,
                    verbose,
                    az_index)
    print("main()...done")


if __name__ == '__main__':
    main()
